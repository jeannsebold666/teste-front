import React, { useState } from 'react';
import CountryDrawer from '../components/CountryDrawer/CountryDrawer';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Drawer } from '@material-ui/core';
import {
  Card,
  Flag,
  Content,
  Row,
  Details,
  CurrencyIcon,
  DetailsTitle,
  DetailsBottom,
  DetailsBottomInner,
  DetailsBottomText,
  DetailsTitleContainer,
  TimeContainer,
  Time,
  MediaDetails,
  MediaDetailsBottom,
  DetailsTop,
  MediaTime,
} from '../components/CountryCard/styles';
import { Small, Caption } from '../components/Typography/Typography';
import Icon from '../components/Icon/Icon';
import { symbols } from '../themes/symbols';
import Spinner from '../components/Spinner/Spinner';
const GET_SINGLE_COUNTRY = gql`
  query Country($id: String) {
    Country(_id: $id) {
      name
      nativeName
      population
      populationDensity
      capital
      subregion {
        name
        region {
          name
        }
      }
      officialLanguages {
        name
      }
      currencies {
        name
        symbol
      }
      flag {
        svgFile
      }
      timezones {
        _id
        name
      }
    }
  }
`;

const ViewCountryTimeZones = ({
  id,
  onClose,
  open,
}: {
  id: number;
  open: boolean;
  onClose: () => void;
}) => {
  const { loading, error, data } = useQuery(GET_SINGLE_COUNTRY, {
    variables: { id },
  });

  console.log(loading);
  console.log(data);
  const test = data;
  let country = [];
  if(data == undefined) {
    console.log(test)
  } else {
    country = test.Country[0]
    console.log('country')
  }
 
  return (
    <>
      <Drawer anchor={'right'} open={open} onClose={onClose}>
      {!country && <Spinner ></Spinner> }
      {country &&<Card>
          <Row>
          {country.flag && <Flag src={country.flag.svgFile} imageSource={country.flag.svgFile} />}
          <Content>
            <Details>
              <DetailsTitleContainer>
                {country.name && <DetailsTitle>{country.name}</DetailsTitle>}
                {country.currency && <CurrencyIcon>{country.currency}</CurrencyIcon>}
              </DetailsTitleContainer>
              <DetailsTop>
                {<Caption>Capital: {country.capital}</Caption>}
                {<Caption>Densidade Demografica: {country.populationDensity}</Caption>}
              </DetailsTop>
              <DetailsBottom>
                {country.population && (
                  <DetailsBottomInner>
                    <Icon type="user" size={symbols.size.iconSmall} />
                    <DetailsBottomText>{country.population}</DetailsBottomText>
                  </DetailsBottomInner>
                )}
                {country.distance && (
                  <DetailsBottomInner>
                    <Icon type="plane" size={symbols.size.iconSmall} />
                    <DetailsBottomText>{country.distance}km</DetailsBottomText>
                  </DetailsBottomInner>
                )}
              </DetailsBottom>
            </Details>
            
          </Content>
        </Row>
      </Card>}
      </Drawer>
      
    </>
  );
};

export default ViewCountryTimeZones;
